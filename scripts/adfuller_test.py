#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import matplotlib.pyplot as plt

from statsmodels.tsa.stattools import adfuller

xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T18.xls')

df = xl.parse("CEI",skiprows=2)

d = { 'Ano' : df['Período'], 'FBCF': df['( - ) Formação bruta de capital'], 'PIB': df['Produto Interno Bruto - PIB'], 'Poup': df['(=) Poupança bruta'], 'CapFina': df['(=) Capacidade / necessidade líquida de financiamento'] }

dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]

X = np.column_stack((dfe_novo.Poup, dfe_novo.CapFina, dfe_novo.PIB))

def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return diff
                                

for i in X.T:
    vezes = 0
    result = adfuller(i)
    while (result[1] > 0.05):
        if (vezes == 0): 
            diff = difference(i, 1)
        else:
            diff = difference(diff, 1)
        vezes = vezes + 1
        result = adfuller(diff)
        
        
    print('Serie Stationary. %f' % vezes )
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))

#
#    else:
#        print('Serie Non-Stationary. ')
#
#
#    result = adfuller(diff)
#    print('ADF Statistic: %f' % result[0])
#    print('p-value: %f' % result[1])
#    if (result[1] <= 0.05):
#        print('Serie Stationary. ')
#    else:
#        print('Serie Non-Stationary. ')

