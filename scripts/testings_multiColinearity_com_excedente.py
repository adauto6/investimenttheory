#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import matplotlib.pyplot as plt

from statsmodels.tsa.stattools import adfuller

from statsmodels.stats.outliers_influence import variance_inflation_factor

import os

xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T18.xls')

df = xl.parse("CEI",skiprows=2)

d = { 'Ano' : df['Período'], 'FBCF': df['( - ) Formação bruta de capital'], 'PIB': df['Produto Interno Bruto - PIB'], 'Poup': df['(=) Poupança bruta'], 'CapFina': df['(=) Capacidade / necessidade líquida de financiamento'] }

dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]

dfe_novo.drop(dfe_novo.tail(6).index,inplace=True)

directory = '../data/'

excedente = list()

for filename in sorted(os.listdir(directory)):
    if filename.endswith(".xls") and filename.startswith("CEI"):
        xl = pd.ExcelFile(directory + filename)
        df = xl.parse("CEI",skiprows=3)
        if (pd.isnull((df['Conta\nde bens\ne serviços\n(usos)'][62]))):
            excedente.append(df['Conta\nde bens\ne serviços\n(usos)'][56])
        else:
            excedente.append(df['Conta\nde bens\ne serviços\n(usos)'][62])
        continue
    else:
        continue

ex_quar = list()

for i in excedente:
   ex_quar.append(i/4)
   ex_quar.append(i/4)
   ex_quar.append(i/4)
   ex_quar.append(i/4)

X = np.column_stack((dfe_novo.FBCF, dfe_novo.Poup, dfe_novo.CapFina, dfe_novo.PIB, ex_quar))

def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return diff

a = np.zeros(shape=(5,2))
maxValueArray=0
qtdVezes=0
new_X = list()


for i in X.T:
    vezes = 0
    result = adfuller(i)
    while (result[1] > 0.05):
        if (vezes == 0): 
            diff = difference(i, 1)
        else:
            diff = difference(diff, 1)
        vezes = vezes + 1
        result = adfuller(diff)
    print('Serie Stationary. %f' % vezes )
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))
    new_X.append(diff)
   
    if ( maxValueArray <= len(diff)):
         maxValueArray = len(diff)

    if ( qtdVezes <= vezes):
         qtdVezes = vezes

X = np.zeros(66)

print(qtdVezes)
print(maxValueArray)

for i in new_X:
    if ( len(i) >= maxValueArray ):
        X = np.column_stack((X, i[0:len(i)-1]))
    else:
        X = np.column_stack((X,i[0:len(i)]))


#X = np.column_stack((dfe_novo.FBCF, dfe_novo.Poup, dfe_novo.CapFina, dfe_novo.PIB))
ar_X = np.column_stack((X[:,2], X[:,3], X[:,4], X[:,5]))

model = sm.OLS( X[:,1], ar_X)

results = model.fit()

print(results.summary(xname=['Poup', 'Capacidade Finan', 'PIB', 'Excedente']))


lar_X = np.column_stack((np.log(X[:,2]), np.log(X[:,3]), np.log(X[:,4])))

lmodel = sm.OLS( np.log(X[:,1]), ar_X)

lresults = model.fit()

print(lresults.summary(xname=['LPoup', 'LCapacidade Finan', 'LPIB', 'Excedente']))

plt.xticks(dfe_novo.index, dfe_novo.Ano, rotation=60, fontsize='x-small')
plt.plot(dfe_novo.index[0:len(dfe_novo.index)-2], results.fittedvalues, 'r--.', label="OLS")
plt.plot(dfe_novo.index[0:len(dfe_novo.index)-2], X[:,1],'o')
plt.legend(loc='upper left')
plt.grid()

nX = pd.DataFrame(ar_X)
vif = pd.DataFrame()

nX.columns = ['Poup', 'CapFina', 'PIB', 'Excedente']

vif["VIF Factor"] = [variance_inflation_factor(nX.values, i) for i in range(nX.shape[1])]

vif["Variaveis"] = nX.columns
print(vif.round(1))

print(np.corrcoef(ar_X.T))

plt.show()

