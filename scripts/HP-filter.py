#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import matplotlib.pyplot as plt


xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T18.xls')

df = xl.parse("CEI",skiprows=2)

d = { 'Ano' : df['Período'], 'FBCF': df['( - ) Formação bruta de capital'], 'PIB': df['Produto Interno Bruto - PIB'], 'Poup': df['(=) Poupança bruta'], 'CapFina': df['(=) Capacidade / necessidade líquida de financiamento'] }

dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]

hp_cycle, hp_trend = sm.tsa.filters.hpfilter(dfe_novo.FBCF, lamb=1600)

plt.plot(hp_trend, label='HP Filter Trend')
plt.plot(dfe_novo.FBCF, label='Sem Processamento')
plt.plot(hp_cycle, label='HP Filter Cycle')
plt.legend(loc='upper left')
plt.grid()

plt.show()
