#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import random

import math

import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import adfuller

from statsmodels.stats.outliers_influence import variance_inflation_factor

import os

from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score


from sklearn.metrics import explained_variance_score
from sklearn.metrics import mean_absolute_error

from sklearn.metrics import r2_score

xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T19.xls')
df = xl.parse("Série Encadeada",skiprows=2)
d = { 'Ano' : df['Período'], 'FBKF': df['Formação Bruta de Capital Fixo'] }

dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]

dfe_novo[[ 'Ano','Trimestre']] = dfe_novo["Ano"].str.split(".", n = 1, expand = True)

dfe_novo["AnoCompleto"]= dfe_novo["Ano"].str.cat(dfe_novo["Trimestre"].copy() , sep =".")

dfe_novo = dfe_novo.dropna()

dfe_novo = dfe_novo.reset_index() ## Reordenando os indices (index)


N = np.size(dfe_novo.FBKF)


x = np.linspace(0.0, N, N)
X = np.arange(0, 94, 1);

#P0029 = np.zeros(N); P0029[00:11] = np.arange(0, 11, 1); # P0029[0:29] = 1
#
#P1029 = np.zeros(N); P1029[11:29] = np.arange(11, 29, 1); #P2948[29:48] = 1 P1029[11:29] = np.arange(1, N-11+1, 1); #P2948[29:48] = 1
#
#P2948 = np.zeros(N); P2948[29:52] = np.arange(29, 52, 1); #P2948[29:48] = 1 P2948[29:48] = np.arange(1, N-29+1, 1); #P2948[29:48] = 1
#
#P4870 = np.zeros(N); P4870[52:70] = np.arange(52, 70, 1); #P2948[29:48] = 1 P4870[52:70] = np.arange(1, N-52+1, 1); #P2948[29:48] = 1
#
#P7085 = np.zeros(N); P7085[70:85] = np.arange(70, 85, 1); #P2948[29:48] = 1 P7085[70:85] = np.arange(1, N-70+1, 1); #P2948[29:48] = 1
#
#P8593 = np.zeros(N); P8593[85:N] = np.arange(85, N, 1); #P2948[29:48] = 1 P8593[85:N] = np.arange(1, N-85+1, 1); #P2948[29:48] = 1


P0029 = np.zeros(N); P0029[00:N] = np.arange(0, N, 1); # P0029[0:29] = 1

#P1029 = np.zeros(N); P1029[11:N] = np.arange(1, N-11+1, 1); #P2948[29:48] = 1

#P2948 = np.zeros(N); P2948[29:N] = np.arange(1, N-29+1, 1); #P2948[29:48] = 1

#P4870 = np.zeros(N); P4870[52:N] = np.arange(1, N-52+1, 1); #P2948[29:48] = 1

P7085 = np.zeros(N); P7085[70:N] = np.arange(1, N-70+1, 1); #P2948[29:48] = 1

#P8593 = np.zeros(N); P8593[85:N] = np.arange(1, N-85+1, 1); #P2948[29:48] = 1

comparacaoHarm = pd.DataFrame()

#for numHarm in range(26,27):

#ar_X = np.column_stack((np.ones(N), np.arange(N), np.sin( 2.0 * np.pi * x/N), np.cos( 2.0 * np.pi * x/N)))

#ar_X = np.column_stack((np.ones(N), P0029, P1029, P2948, P4870, P7085, P8593, np.sin( 2.0 * np.pi * x/N), np.cos( 2.0 * np.pi * x/N)))

#ar_X = np.column_stack((np.ones(N), P0029, P7085, P8593, np.sin( 2.0 * np.pi * x/N), np.cos( 2.0 * np.pi * x/N)))


StrnumHarm = ['const', 'P96.I', 'P98.II', 'P03.II', 'P09.I','P13.III', 'P17.II', 'sin1', 'cos1']
StrnumHarm = ['const', 'P96.I', 'P13.III', 'sin1', 'cos1']

acc_score = []

kf = KFold(n_splits=3,shuffle=True, random_state=2)

#results.params[results.pvalues<0.05]

#acc_score.append(accuracy_score(predictions, y_test))

#np.mean(acc_score)

numHarm=7
for numHarm in range(3,32):
#    ar_X = np.column_stack((np.ones(N), P0029, P1029, P2948, P4870, P7085, P8593, np.sin( 2.0 * np.pi * x/N), np.cos( 2.0 * np.pi * x/N)))
    ar_X = np.column_stack((np.ones(N), P0029, P7085, np.sin( 2.0 * np.pi * x/N), np.cos( 2.0 * np.pi * x/N)))
#    StrnumHarm = ['const', 'P96.I', 'P98.II', 'P03.II', 'P09.I','P13.III', 'P17.II', 'sin1', 'cos1']
    StrnumHarm = ['const', 'P96.I', 'P13.III', 'sin1', 'cos1']
    for i in range(2,numHarm):
        y = ((np.sin( i * 2.0*np.pi*x/N))).reshape(N,1)
        StrnumHarm.append('sin' + str(i))
        ar_X = np.hstack((ar_X, y))
        y = ((np.cos( i * 2.0*np.pi*x/N))).reshape(N,1)
        StrnumHarm.append('cos' + str(i))
        ar_X = np.hstack((ar_X, y))
        

    for train_index, test_index in kf.split(ar_X, dfe_novo.FBKF):
        X_train, X_test = ar_X[train_index], ar_X[test_index]
        y_train, y_test = dfe_novo.FBKF[train_index], dfe_novo.FBKF[test_index]

    model = sm.OLS( y_train, X_train)
    results = model.fit()
    predictions = results.predict(X_test)

#    model = sm.OLS( dfe_novo.FBKF, ar_X)
#    results = model.fit()

    if (numHarm == 5):
        print(y_test)
        print(predictions)

        print(results.summary(xname=StrnumHarm))
        Melhormodel = model # sm.OLS( dfe_novo.FBKF, ar_X)
        Melhorresults = Melhormodel.fit()
        Melhorar_X = ar_X
        
        print(Melhorresults.params[Melhorresults.pvalues<0.05])

        print(Melhorresults.summary(xname=StrnumHarm))

    #
    result = adfuller(results.resid)
    #print('ADF Statistic: %f' % result[0])
    #print('p-value: %f' % result[1])
    #for key, value in result[4].items():
    #	print('\t%s: %.3f' % (key, value))
    #
    #if ( result[1] > 0.05 ):
    #	print('Residual is NOT Stationary')

    if (explained_variance_score(y_test, predictions) > 0.7):
        comparacaoHarm[numHarm] = np.array ( [numHarm, sum(results.pvalues < 0.05), results.fvalue, results.rsquared, result[1]<0.05, explained_variance_score(y_test, predictions),  mean_absolute_error(predictions, y_test), r2_score(y_test, predictions)   ])
        #    comparacaoHarm[numHarm] = np.array ( [numHarm, sum(results.pvalues < 0.05), results.fvalue, results.rsquared, result[1]<0.05, explained_variance_score(y_test, predictions),  mean_absolute_error(predictions, y_test), r2_score(y_test, predictions)   ])

#    comparacaoHarm[numHarm] = np.array ( [numHarm, sum(results.pvalues < 0.05), results.fvalue, results.rsquared, result[1]<0.05])

comparacaoHarm = comparacaoHarm.T

comparacaoHarm.columns=['Qtd Harmônicos','Qtd Coef. Significantes 5%','Estatistica F','R^2','Residuous Normais', 'Variância Explicada', 'Erro médio absoluto', 'R2']
comparacaoHarm.reset_index()

#print(comparacaoHarm.sort_values(by=['Variância Explicada']))


print(comparacaoHarm.sort_values(by=['Estatistica F','Qtd Coef. Significantes 5%'],ascending=False))

fig, (ax1, ax2) = plt.subplots(2)

#ax3.plot(np.array(y_test))
#ax3.plot(predictions)

plt.xticks(dfe_novo.index, dfe_novo.AnoCompleto, rotation=60, fontsize='x-small')
plt.xticks(dfe_novo.index[::2], dfe_novo.AnoCompleto[::2], rotation=60, size=10)

#ax2 = ax1[1].twinx()
#ax2 = ax1[1]
axx = ax2.twiny()

lns1 = ax1.plot(dfe_novo.index, dfe_novo.FBKF, 'r--.', label="Formação Bruta de Capital Fixo")

#lns2 = ax1.plot(dfe_novo.index, Melhorresults.fittedvalues  , 'b--.', label="MQO")

lns2 = ax1.plot(dfe_novo.index, Melhorresults.predict(Melhorar_X)  , 'b--.', label="MQO")

#lns2 = ax1.plot(dfe_novo.index, 1.511*np.arange(1,95,1) + 11.69 * np.sin( 2 * 2.0 * np.pi * x/N) + 9.33 * np.cos( 4 * 2.0 * np.pi * x/N) + 5.25* np.sin( 5 * 2.0 * np.pi * x/N) + 4.41 * np.cos( 6 * 2.0 * np.pi * x/N) , 'b--.', label="Significante")

#lns2 = ax2.plot(dfe_novo.index, -39.9608 * np.sin(     2.0 * np.pi * x/N) , 'b--.', label="sin")
#ns3 = ax2.plot(dfe_novo.index,   34.7849 * np.cos( 1 * 2.0 * np.pi * x/N) , 'g--.', label="cos1")

lns5 = ax2.plot(dfe_novo.index,   12.6103 * np.sin( 2 * 2.0 * np.pi * x/N) , 'm--.', label="sin2")
#
lns6 = ax2.plot(dfe_novo.index,    5.8710 * np.sin( 3 * 2.0 * np.pi * x/N) , 'y--.', label="sin3")
#
lns7 = ax2.plot(dfe_novo.index,    3.4990 * np.cos( 3 * 2.0 * np.pi * x/N) , 'k--.', label="cos3")

lns8 = ax2.plot(dfe_novo.index,    2.4136 * np.sin( 4 * 2.0 * np.pi * x/N) , 'g--.', label="sin4")

lns4 = ax2.plot(dfe_novo.index,    4.5207 * np.cos( 4 * 2.0 * np.pi * x/N) , 'c--.', label="cos4")

#lns3 = ax2.plot(dfe_novo.index,    3.4601 * np.cos( 5 * 2.0 * np.pi * x/N) , 'g--.', label="cos5")

lns = lns1+lns2+lns4+lns5+lns6+lns7+lns8
labs = [l.get_label() for l in lns]

axx.xaxis.set_ticks_position('bottom') # set the position of the second x-axis to bottom

axx.spines['bottom'].set_position(('outward', 36))

axx.xaxis.set_ticks(np.arange(0, 93, 2))

axx.set_xlim(ax1.get_xlim())

ax1.legend(lns, labs, loc='upper left')

plt.grid()

plt.show(block=False)
     


#
### dfe_novo['MM'] = dfe_novo['PIB'].rolling(2, win_type='triang').sum() Não consegui colocar a janela correta.
#
#kernel = np.array([0.5,1,1,1,0.5])/4
#
#dfe_novo['MM'] =  pd.DataFrame(np.convolve(dfe_novo['PIB'],kernel,'valid'))
#
#dfe_novo.drop(['index'], axis=1)
#
#dfe_novo = dfe_novo.reset_index() ## Reordenando os indices (index)
#
#dfe_novo['MM'] = dfe_novo.MM.shift(2)
#
#dfe_novo['div'] = 0
#
#dfe_novo['div'] = dfe_novo['div'].astype(float)
#
#for index, row in dfe_novo.iterrows():
#    if not math.isnan(row['MM']):
#        dfe_novo.at[index, 'div'] = row['PIB']/row['MM']
#    else:
#        dfe_novo.at[index, 'div'] = 'NaN'
#
#primeiro = np.mean(dfe_novo[dfe_novo['Trimestre'] == "I"]['div'].dropna())
#
#segundo = np.mean(dfe_novo[dfe_novo['Trimestre'] == "II"]['div'].dropna())
#
#terceiro = np.mean(dfe_novo[dfe_novo['Trimestre'] == "III"]['div'].dropna())
#
#quarto = np.mean(dfe_novo[dfe_novo['Trimestre'] == "IV"]['div'].dropna())
#
#estoqSazo = pd.DataFrame(0, index=range(4*len(dfest)), columns=range(1))
#
#estoqSazo.columns = ["valor"]
#
#dfest = dfest.dropna()
#
#dfest.columns = ["Ano","Const", "Maq", "Outros", "Total"]
#
#dfest = dfest.reset_index()
#
#for index, row in dfest.iterrows():
#    estoqSazo.at[index*4, "valor"] = primeiro*row['Maq']
#    estoqSazo.at[(index*4)+1, "valor"] = segundo*row['Maq']
#    estoqSazo.at[(index*4)+2, "valor"] = terceiro*row['Maq']
#    estoqSazo.at[(index*4)+3, "valor"] = quarto*row['Maq']
#
