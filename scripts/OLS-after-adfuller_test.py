#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import matplotlib.pyplot as plt

from statsmodels.tsa.stattools import adfuller

xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T18.xls')

df = xl.parse("CEI",skiprows=2)

d = { 'Ano' : df['Período'], 'FBCF': df['( - ) Formação bruta de capital'], 'PIB': df['Produto Interno Bruto - PIB'], 'Poup': df['(=) Poupança bruta'], 'CapFina': df['(=) Capacidade / necessidade líquida de financiamento'] }

dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]

X = np.column_stack((dfe_novo.FBCF, dfe_novo.Poup, dfe_novo.CapFina, dfe_novo.PIB))

def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return diff

a = np.zeros(shape=(5,2))
maxValueArray=0
qtdVezes=0
new_X = list()


for i in X.T:
    vezes = 0
    result = adfuller(i)
    while (result[1] > 0.05):
        if (vezes == 0): 
            diff = difference(i, 1)
        else:
            diff = difference(diff, 1)
        vezes = vezes + 1
        result = adfuller(diff)
    print('Serie Stationary. %f' % vezes )
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))
    new_X.append(diff)
   
    if ( maxValueArray <= len(diff)):
         maxValueArray = len(diff)

    if ( qtdVezes <= vezes):
         qtdVezes = vezes

X = np.zeros(72)

print(qtdVezes)
print(maxValueArray)

for i in new_X:
    if ( len(i) >= maxValueArray ):
        X = np.column_stack((X, i[0:len(i)-1]))
    else:
        X = np.column_stack((X,i[0:len(i)]))


#X = np.column_stack((dfe_novo.FBCF, dfe_novo.Poup, dfe_novo.CapFina, dfe_novo.PIB))
ar_X = np.column_stack((X[:,2], X[:,3], X[:,4]))

model = sm.OLS( X[:,1], ar_X)

results = model.fit()

print(results.summary())
    
plt.xticks(dfe_novo.index, dfe_novo.Ano, rotation=60, fontsize='x-small')
plt.plot(dfe_novo.index[0:len(dfe_novo.index)-2], results.fittedvalues, 'r--.', label="OLS")
plt.plot(dfe_novo.index[0:len(dfe_novo.index)-2], X[:,1],'o')
plt.legend(loc='upper left')
plt.grid()

plt.show()
