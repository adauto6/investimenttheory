#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import matplotlib.pyplot as plt

xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T18.xls')

df = xl.parse("CEI",skiprows=2)
d = { 'Ano' : df['Período'], 'FBCF': df['( - ) Formação bruta de capital'], 'PIB': df['Produto Interno Bruto - PIB'], 'Poup': df['(=) Poupança bruta'], 'CapFina': df['(=) Capacidade / necessidade líquida de financiamento'] }
dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]
X = np.column_stack((dfe_novo.Poup, dfe_novo.CapFina, dfe_novo.PIB))


model = sm.OLS(dfe_novo.FBCF, X)

results = model.fit()

print(results.summary())

#print('Predicted values: ', results.predict())

plt.xticks(dfe_novo.index, dfe_novo.Ano, rotation=60, fontsize='x-small')
plt.plot(dfe_novo.index, results.fittedvalues, 'r--.', label="OLS")
plt.plot(dfe_novo.index, dfe_novo.FBCF,'o')
plt.legend(loc='upper left')
plt.grid()

plt.show()

