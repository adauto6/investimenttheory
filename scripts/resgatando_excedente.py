#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import matplotlib.pyplot as plt

from statsmodels.tsa.stattools import adfuller

from statsmodels.stats.outliers_influence import variance_inflation_factor

import os

directory = '../data/'

excedente = list()


for filename in sorted(os.listdir(directory)):
    if filename.endswith(".xls") and filename.startswith("CEI"):
        xl = pd.ExcelFile(directory + filename)
        df = xl.parse("CEI",skiprows=3)
        if (pd.isnull((df['Conta\nde bens\ne serviços\n(usos)'][62]))):
            excedente.append(df['Conta\nde bens\ne serviços\n(usos)'][56])
        else:
            excedente.append(df['Conta\nde bens\ne serviços\n(usos)'][62])
        continue
    else:
        continue

ex_quar = list()

for i in excedente:
   ex_quar.append(i/4)
   ex_quar.append(i/4)
   ex_quar.append(i/4)
   ex_quar.append(i/4)


print(len(ex_quar))

X = np.column_stack((ex_quar,ex_quar))

def difference(dataset, interval=1):
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return diff

maxValueArray=0
qtdVezes=0
new_X = list()


for i in X.T:
    vezes = 0
    result = adfuller(i)
    while (result[1] > 0.05):
        if (vezes == 0): 
            diff = difference(i, 1)
        else:
            diff = difference(diff, 1)
        vezes = vezes + 1
        result = adfuller(diff)
    print('Serie Stationary. %f' % vezes )
    print('ADF Statistic: %f' % result[0])
    print('p-value: %f' % result[1])
    print('Critical Values:')
    for key, value in result[4].items():
        print('\t%s: %.3f' % (key, value))
    new_X.append(diff)
   
    if ( maxValueArray <= len(diff)):
         maxValueArray = len(diff)

    if ( qtdVezes <= vezes):
         qtdVezes = vezes
