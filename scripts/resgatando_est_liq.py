#!/usr/bin/env python

import numpy as np
import pandas as pd
import statsmodels.api as sm

import math

import matplotlib.pyplot as plt

from statsmodels.tsa.stattools import adfuller

from statsmodels.stats.outliers_influence import variance_inflation_factor

import os

directory = '../data/'
filename  = 'EKL.xlsx'
dfest = pd.read_excel(directory + filename, skiprows=3, header=0)

indexNames = dfest[ dfest['Ano'] < 2000 ].index

dfest.drop(indexNames , inplace=True)

dfest = dfest.dropna()

## Resgatando Serie do PIB para sazonalizar.

xl = pd.ExcelFile('../data/Tab_Compl_CNT_2T18.xls')
df = xl.parse("CEI",skiprows=2)
d = { 'Ano' : df['Período'], 'PIB': df['Produto Interno Bruto - PIB'] }

dfe = pd.DataFrame(d)
dfe_novo = dfe[dfe.Ano.str.contains(r'.', na=False)]
dfe_novo = dfe_novo[~dfe_novo.Ano.str.contains(r'Esta', na=False)]

dfe_novo.drop(dfe_novo.tail(14).index,inplace=True)

dfe_novo[[ 'Ano','Trimestre']] = dfe_novo["Ano"].str.split(".", n = 1, expand = True)


dfe_novo = dfe_novo.reset_index() ## Reordenando os indices (index)

## dfe_novo['MM'] = dfe_novo['PIB'].rolling(2, win_type='triang').sum() Não consegui colocar a janela correta.

kernel = np.array([0.5,1,1,1,0.5])/4

dfe_novo['MM'] =  pd.DataFrame(np.convolve(dfe_novo['PIB'],kernel,'valid'))

dfe_novo.drop(['index'], axis=1)

dfe_novo = dfe_novo.reset_index() ## Reordenando os indices (index)

dfe_novo['MM'] = dfe_novo.MM.shift(2)

dfe_novo['div'] = 0

dfe_novo['div'] = dfe_novo['div'].astype(float)

for index, row in dfe_novo.iterrows():
    if not math.isnan(row['MM']):
        dfe_novo.at[index, 'div'] = row['PIB']/row['MM']
    else:
        dfe_novo.at[index, 'div'] = 'NaN'

primeiro = np.mean(dfe_novo[dfe_novo['Trimestre'] == "I"]['div'].dropna())

segundo = np.mean(dfe_novo[dfe_novo['Trimestre'] == "II"]['div'].dropna())

terceiro = np.mean(dfe_novo[dfe_novo['Trimestre'] == "III"]['div'].dropna())

quarto = np.mean(dfe_novo[dfe_novo['Trimestre'] == "IV"]['div'].dropna())

estoqSazo = pd.DataFrame(0, index=range(4*len(dfest)), columns=range(1))

estoqSazo.columns = ["valor"]

dfest = dfest.dropna()

dfest.columns = ["Ano","Const", "Maq", "Outros", "Total"]

dfest = dfest.reset_index()

for index, row in dfest.iterrows():
    estoqSazo.at[index*4, "valor"] = primeiro*row['Maq']
    estoqSazo.at[(index*4)+1, "valor"] = segundo*row['Maq']
    estoqSazo.at[(index*4)+2, "valor"] = terceiro*row['Maq']
    estoqSazo.at[(index*4)+3, "valor"] = quarto*row['Maq']

dfe_novo["AnoCompleto"]= dfe_novo["Ano"].str.cat(dfe_novo["Trimestre"].copy() , sep =".")

plt.xticks(dfe_novo.index, dfe_novo.AnoCompleto, rotation=60, fontsize='x-small')
plt.plot(dfe_novo.index[0:len(dfe_novo.index)], estoqSazo.valor, 'r--.', label="Estoque Sazonal")
plt.legend(loc='upper left')
plt.grid()

plt.savefig('estoq_liquido_mimi.png')

plt.show()
